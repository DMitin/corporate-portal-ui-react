var webpack = require('webpack');

/*
 * Default webpack configuration for development
 */
var config = {
  devtool: "source-map",
  entry:  __dirname + "/src/js/App.js",
  output: {
    path: __dirname + "/public",
    filename: "bundle.js"
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['es2015','react']
      }
    },
      { test: /\.css$/, loader: "style-loader!css-loader" },
      { test: /\.png$/, loader: "url-loader?limit=100000" },
      { test: /\.jpg$/, loader: "file-loader" }
    ]
  },/*
  proxy: {
    '/rest/*': {
      target: 'http://localhost:8080/',
      secure: false
    }
  },
  */
  devServer: {
    contentBase: "./public",
    colors: true,
    historyApiFallback: true,
    inline: true,
    proxy: {
      '/rest/*': {
        target: 'http://localhost:8080/',
        secure: false
      },
      '/images/*': {
        target: 'http://localhost:8080/',
        secure: false
      }
    }
  },
}

/*
 * If bundling for production, optimize output
 */
if (process.env.NODE_ENV === 'production') {
  config.devtool = false;
  config.plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({comments: false}),
    new webpack.DefinePlugin({
      'process.env': {NODE_ENV: JSON.stringify('production')}
    })
  ];
};

module.exports = config;
