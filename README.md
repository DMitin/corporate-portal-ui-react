Corporate-Portal-UI-Frontend
------------------------------

# Setting up an environment
```
npm i
npm i -g gulp
npm i -g webpack
npm run build-styles
npm run start
```
http://localhost:8081/

# Build UI
## Build Styles

To build styles run
```
npm run build-styles
```

To run watcher run
```
npm run watch-styles
```
## Build JS

To build scripts manualy run
```
npm run build-scripts
```

# Поддерживаемые браузеры
- Chrome
- Mozilla (Not Tested)
- Safari (In Progress)