import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import * as profileActions from './actions/profile';

class Profile extends Component {


	componentDidMount() {
		const {store} = this.context;
		console.log("get user info");
		let { userId } = this.props.params;
		if (userId) {
			store.dispatch(profileActions.loadProfile(userId));
		} else {
			store.dispatch(profileActions.viewProfile());
		}


	}

	render(){
		console.log(this.props);
		const {store} = this.context;
		let { userId } = this.props.params;
		let profileId = store.getState().profile.id;

		let imagePath;
		if (this.props.image) {
			imagePath = this.props.image;
		} else {
			imagePath = "/static/Content/images/no-image.png"
		}
		//let editable = false;
		let editable;
		if (!userId || parseInt(userId) === profileId) {
			editable = <Link id="edit-anchor" className="pull-right edit-profile-btn" to="/edit">Редактировать</Link>;
		}

		//console.log(this.props);
		let social =  this.props.social || {};
		return (
			<div>

				<div className="link-back">
					<Link className="icn-angle-double-left" to="/">&nbsp;<u>Назад к списку сотрудников</u></Link>
				</div>

				<h2>{this.props.lastName} {this.props.firstName} {this.props.middleName}
					{editable}
				</h2>




				<div className="search-item">
					<div><span><img src={imagePath}/></span></div>
					<div className="personal-info">
						<div className="position">Front-end разработчик</div>
						<div className="email"><strong>Email:</strong> <a href="mailto:{this.props.email}">{this.props.email}</a></div>
						<div className="skype"><strong>Skype:</strong> {this.props.skype}</div>

						<div className="department"><strong>Отдел:</strong> xxx</div>
						<div className="projects"><strong>Проекты:</strong> <a>xxx</a></div>
					</div>
					<div className="align-center map">
						<a className="icn-map-marker" title="Рабочее место"><u>xxx</u></a>
					</div>
				</div>


				<div className="block-title">Дополнительная информация</div>

				<ul className="social">
					<li><a className="icn-twitter"><u>{social.twitter}</u></a></li>
					<li><a className="icn-facebook"><u>{social.facebook}</u></a></li>
					<li><a className="icn-google-plus"><u>{social.googlePlus}</u></a></li>
					<li><a className="icn-linkedin"><u>{social.linkedin}</u></a></li>
					<li><a className="icn-instagram"><u>{social.instagram}</u></a></li>
					<li><a className="icn-vk"><u>{social.vk}</u></a></li>
				</ul>


				<div className="block-title">Навыки</div>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>

			</div>

		);
	}
}
//export default Profile;

Profile.contextTypes = {
	store: React.PropTypes.object
};

//export default Search;
export default connect(
	state => (state.watchProfile)
)(Profile);
