import React, { Component } from 'react';
import { connect } from 'react-redux';

import actions from './actions';

class Page extends Component {
	render(){
		//let className = "page";
		const {store} = this.context;
		let active = this.props.active;
		let number = this.props.number;
		let className = active ? "page active" : "page" ;

		return (
			<li>
				<a className={className}
						onClick={(e)=>{
								//store.dispatch(actions.firstPage());

								//console.log("change page " + number );
								store.dispatch(actions.changePage(number));
						}}>{number}
				</a>
			</li>
		)
	}
}

//export default Page;

Page.contextTypes = {
	store: React.PropTypes.object
};



export default connect(
	state => (state)
)(Page);