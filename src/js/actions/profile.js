import api from '../api.js';
import { profileTypes } from './actionTypes';

/*
const loadProfile = (id) => {
  return {
  	type: 'LOAD_PROFILE'
  };
};
*/

export function loadProfile(id) {
	return (dispatch, getState) => {
		api.userInfo(id)
			.then(response => {
				dispatch({
			        type: profileTypes.WATCH,
			        data: response
			    });
			});
	}
};

export function viewProfile() {
	return (dispatch, getState) => {
		let profile =  getState().profile;
		console.log(profile);
		dispatch({
			type: profileTypes.WATCH,
			data: profile
		});
	}
};


export function updateProfile(profile) {
	return (dispatch, getState) => {
		api.updateProfile(profile)
			.then(response => {
				dispatch({
					type: profileTypes.UPDATE,
					data: response
				});
			});
	}
};


export function uploadImage(body) {
	return (dispatch, getState) => {
		api.loadImage(body)
			.then(response => {
				dispatch({
					type: profileTypes.UPDATE,
					data: response
				});
			});
	}
};
