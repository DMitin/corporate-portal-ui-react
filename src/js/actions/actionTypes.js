export const profileTypes = {
  WATCH: 'PROFILE_WATCH',
  UPDATE: 'PROFILE_UPDATE'
}
