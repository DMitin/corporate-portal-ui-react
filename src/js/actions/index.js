import api from '../api.js';

const handleSearchResponse = (dispatch, searchString, pageNumber, desc) => {
	return response => {
		dispatch({
	        type: 'SEARCH_SUCCESS',
	        data: {
				desc: desc,
	        	response: response,
	        	searchString: searchString,
	        	pageNumber: pageNumber
	        }
	    });	
	}	
};

const search = (searchString = "", pageNumber = 0) => {
	return (dispatch, getState) => {
		let pageSize = getState().search.pageSize;
		let desc = getState().search.desc;
		api.search(searchString, pageNumber, pageSize, desc)
			.then(handleSearchResponse(dispatch, searchString, pageNumber, desc));
	}
};

const firstPage = () => {
	return (dispatch, getState) => {
		let searchString = getState().search.searchString;
		let pageNumber = 0;
		let pageSize = getState().search.pageSize;
		let desc = getState().search.desc;
		api.search(searchString, pageNumber, pageSize, desc)
			.then(handleSearchResponse(dispatch, searchString, pageNumber, desc));
		
	}
	console.log("SHOW_FIRST_PAGE");
};

const lastPage = () => {
	return (dispatch, getState) => {
		let searchString = getState().search.searchString;
		let pageNumber = getState().search.pageCount - 1;
		let pageSize = getState().search.pageSize;
		let desc = getState().search.desc;
		api.search(searchString, pageNumber, pageSize, desc)
			.then(handleSearchResponse(dispatch, searchString, pageNumber, desc));
		
	}
	console.log("SHOW_FIRST_PAGE");
};

const changePage = (pageNumber) => {	
	return (dispatch, getState) => {
		let searchString = getState().search.searchString;
		let pageSize = getState().search.pageSize;
		let desc = getState().search.desc;
		api.search(searchString, pageNumber, pageSize, desc)
			.then(handleSearchResponse(dispatch, searchString, pageNumber, desc));
	}
	
	console.log("show page: " + pageNumber);
};
/*
const nextPage = (pageNumber) => {
	return (dispatch, getState) => {
		let searchString = getState().search.searchString;
		let pageSize = getState().search.pageSize;
		api.search(searchString, pageNumber, pageSize)
			.then(handleSearchResponse(dispatch, searchString, pageNumber));
	}

	console.log("show page: " + pageNumber);
};
*/
const changePageSize = (pageSize) => {
	return (dispatch, getState) => {
		let searchString = getState().search.searchString;
		let pageNumber = 0;
		let desc = getState().search.desc;
		api.search(searchString, pageNumber, pageSize, desc)
			.then(handleSearchResponse(dispatch, searchString, pageNumber, desc));
	}

	console.log("changePageSize: " + pageNumber);
};

const nextPage = () => {
	return (dispatch, getState) => {
		let searchString = getState().search.searchString;
		let maxPage = getState().search.pageCount - 1;
		let currentPage = getState().search.currentPage;
		let pageSize = getState().search.pageSize;
		let desc = getState().search.desc;
		if (currentPage < maxPage) {
			let pageNumber = currentPage + 1;
			api.search(searchString, pageNumber, pageSize, desc)
				.then(handleSearchResponse(dispatch, searchString, pageNumber, desc));
		}
	};

	console.log("nextPage: " + pageNumber);
};


const previousPage = () => {
	return (dispatch, getState) => {
		let searchString = getState().search.searchString;
		let currentPage = getState().search.currentPage;
		let pageSize = getState().search.pageSize;
		let desc = getState().search.desc;
		if (currentPage > 0) {
			let pageNumber = currentPage - 1;
			api.search(searchString, pageNumber, pageSize, desc)
				.then(handleSearchResponse(dispatch, searchString, pageNumber, desc));
		}
	};

	console.log("nextPage: " + pageNumber);
};

const changeDesc = () => {
	return (dispatch, getState) => {
		let desc = getState().search.desc;
		let toggleDesc = !desc;
		let searchString = getState().search.searchString;
		let currentPage = getState().search.currentPage;
		let pageSize = getState().search.pageSize;
		api.search(searchString, currentPage, pageSize, toggleDesc)
			.then(handleSearchResponse(dispatch, searchString, currentPage, toggleDesc));

	};
};

//const userInfo




export default {
  	search,
  	firstPage,
  	lastPage,
	nextPage,
	previousPage,
  	changePage,
	changePageSize,
	changeDesc
};
