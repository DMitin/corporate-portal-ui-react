/**
 * Created by Denis Mitin on 26.04.2016.
 */
 var testMethod = () => {
    console.log("testMethod");
};


var login = (username, password) => {
    return fetch("/rest/auth/login", {
        method: 'post',
        credentials:"include",
        body: JSON.stringify({
            ldapName: username,
            password: password
        })
    }).then(function(response) {
        return response.json()
    });
};

var logout = () => {
    return fetch("/rest/auth/logout", {
        method: 'post'
    }).then(function(response) {
        return response
    });
};

var search = (searchStr, pageNumber, pageSize, desc) => {
    return fetch("/rest/user/search", {
        method: 'post',
        credentials:"include",
        body: JSON.stringify({
            searchString: searchStr,
            pageNumber: pageNumber,
            pageSize: pageSize,
            desc: desc
        })
    }).then(function(response) {
        return response.json()
    });
};

var userInfo = (userId) => {
    return fetch("/rest/user/" + userId, {
        method: 'get',
        credentials:"include"
    }).then(function(response) {
        return response.json()
    });
};

var updateProfile = (profile) => {
    return fetch("/rest/user/", {
        method: 'put',
        credentials:"include",
        body: JSON.stringify(profile)
    }).then(function(response) {
        return response.json()
    });
};


var loadImage = (body) => {
    return fetch("/rest/user/loadImage", {
        method: 'post',
        credentials:"include",
        body: body
    }).then(function(response) {
        return response.json()
    });
};

export default {
    testMethod,
    userInfo,
    login,
    logout,
    search,
    updateProfile,
    loadImage
};