import React, { Component } from 'react';
import { connect } from 'react-redux';

import api from './api.js';


class Login extends Component {

	constructor(props) {
	  	super(props);
	  	this.tick = this.tick.bind(this);
	  	this.login = this.login.bind(this);
	  	this.logout = this.logout.bind(this);
	  	this.state = {
	  		login: "",
	  		password: ""
	  	};
	}

	componentDidMount() {
		this.login();
	}

	tick(e) {
	    this.setState({login: e.target.value});
	}

	login() {

		const {store} = this.context;
		const {router} = this.context;
		const { location } = this.props;

		console.log(this.state);
		api.login(this.state.login, this.state.password).then(function(response) {
		    console.log(response);
		    store.dispatch({
		        type: 'AUTH_SUCCESS',
		        data: response
		    });

			if (location.state && location.state.nextPathname) {
				if(response.status === "new") {
					router.replace("/card");
				} else {
					router.replace(location.state.nextPathname)
				}
			} else {
				if(response.status === "new") {
					router.replace("/card");
				} else {
					router.replace('/');
				}
			}
		});
	}

	logout() {
		api.logout().then(function(response) {
			console.log("Пользователь вышел");
		});
	}

	render(){

		return (
			<div className="login-wrapper">
				<span className="login-logo"></span>
				<div className="login-form">
					<h3>Добро пожаловать на портал!</h3>
					<input id="login-input" type="text" value={this.state.login} placeholder="Ваш логин (учетная запись)"
						   onChange={this.tick} />
					<input id="password-input" type="password" value={this.state.password} placeholder="Пароль"
						   onChange={(e)=>{
								this.setState({password: e.target.value});
						   }}/>
					<button id="login-button" className="btn btn-primary" onClick={this.login}>Войти</button>
				</div>
			</div>
		)
	}
}

Login.contextTypes = {
	store: React.PropTypes.object,
	router: React.PropTypes.object
};

export default connect(
	state => (state.search)
)(Login);
