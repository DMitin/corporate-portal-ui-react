import { profileTypes } from '../actions/actionTypes';

export default (state = {}, action) => {

    let resp;
    switch (action.type) {
        case profileTypes.WATCH:
            resp = Object.assign({}, action.data);
            console.log("WATCH_PROFILE: " +  resp);
            return resp;
            break;
        case profileTypes.UPDATE:
            resp = Object.assign({}, action.data);
            console.log("WATCH_PROFILE: " +  JSON.stringify(resp));
            return resp;
            break;
        default:
            return state;
    }
};
