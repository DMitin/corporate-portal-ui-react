/**
 * Created by Denis Mitin on 27.04.2016.
 */

/*
export default (state = {}, action) => {
    switch (action.type) {
        case 'SEARCH':
            return Object.assign({}, action.data, {
                page: 0
            });
            break;
    }
};
    */

//import Config from '../config';
let initialState = {
    pageSize: 5,
    desc: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SEARCH_SUCCESS':
            let searchResponse = action.data.response;
            //console.log("SEARCH_SUCCESS" + JSON.stringify(action.data, null, 2));

            let pageCount = Math.floor(searchResponse.totalRecords/searchResponse.pageSize);
            if ((searchResponse.totalRecords % searchResponse.pageSize) !== 0) {
                pageCount ++;
            } 


            let resp = Object.assign({}, {
                pageSize: searchResponse.pageSize,
                desc: action.data.desc,
                currentPage: action.data.pageNumber,
                totalRecords: searchResponse.totalRecords,
                pageCount: pageCount,
                searchResults: searchResponse.searchResult,
                searchString: action.data.searchString
            });
            console.log(resp);
            return resp;
            break;
        case 'FIRST_PAGE':
            break;
        default:
            return state;
    }
};  