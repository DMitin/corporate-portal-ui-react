import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import profile from './profile';
import search from './search';
import watchProfile from './watchProfile';
import panel from './panel';


export default combineReducers({
  profile,
  search,
  watchProfile,
  routing: routerReducer,
  panel
});
