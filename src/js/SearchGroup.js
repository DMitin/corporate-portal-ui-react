import React, { Component } from 'react';
import SearchItem from './SearchItem';

class SearchGroup extends Component {
	render(){

		var items = this.props.items.map((item) => {
			let fullName = item.firstName + " " + item.middleName + " " + item.lastName;
			if( !fullName.trim() ) {
				fullName = "НЕ ЗАДАНО!!!";
			}
			let imagePath;
			if (item.image) {
				imagePath = item.image;
			} else {
				imagePath = "/static/Content/images/no-image.png"
			}
			return <SearchItem key={item.id}
						id={item.id}
						name={fullName}
						position={item.position}
						email={item.email}
						skype={item.skype}
						room={item.room}
					    image={imagePath}
						 />
		});
		return (
			<div className="search-group">
				<div className="letter">{this.props.letter}</div>
				{items}
			</div>			
		)
	};
}

export default SearchGroup;