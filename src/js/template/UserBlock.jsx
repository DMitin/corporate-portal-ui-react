import React from 'react';
import { connect } from 'react-redux';

const UserBlock = ({
  profile
}) => (
  <div className="user-block">
    {profile.image ? (
      <div className="user-block__image" style={{
        backgroundImage: `url(${profile.image})`
      }}>
        <img src={profile.image}/>
      </div>
    ) : (
      <label className="fake-file icn-camera"></label>
    )}
    <div className="welcome">Добрый день,  {profile.length ? profile.firstName : 'Username'}!</div>
  </div>
);

export default connect(
  state => {
    return {
      profile: state.profile
    }
  }
)(UserBlock);
