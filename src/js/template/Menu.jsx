import React from 'react';
import { Link } from 'react-router';
// 
// <li><a className="icn-map-marker"><span>План рассадки</span></a></li>
// <li><a className="icn-briefcase"><span>Проекты</span></a></li>
// <li><a className="icn-info"><span>Информация для сотрудника</span></a></li>

export default () => {

  // TODO: Make an active state for links. class 'active'. Do we have styles for that?
  return (
    <ul>
      <li>
        <Link to="/" className='icn-user active'>
          <span>Сотрудники</span>
        </Link>
      </li>
      <li>
        <Link to="card" className='icn-cog'>
          <span>Настройки профиля</span>
        </Link>
      </li>
    </ul>
  );
};
