import React, { Component } from 'react';
import { connect } from 'react-redux';
import Footer from './Footer.jsx';
import Menu from './Menu.jsx';
import UserBlock from './UserBlock.jsx';

class Template extends Component {
    render() {
        const {store} = this.context;
        var isOpen = store.getState().panel;
        var className = isOpen ? "aside open" : "aside";
        console.log(className);
        return (
            <div className="page-wrapper">
                <div className={className}>
                    <div className="inner">

                        <div className="scroll">
                            <div className={className}>


                                <a className="logo"></a>

                                <UserBlock/>

                                <Menu/>

                                <a className="toggle" onClick={(e)=>{
                                    console.log("change menu");
                                    var panel = store.getState().panel;
                                    console.log(panel);
                                    store.dispatch({
                                        type:"TOGGLE_PANEL",
                                        data: !panel
                                    });
                               }}></a>

                            </div>
                        </div>

                    </div>
                </div>
                <div className="wrapper">
                    <div className="content">
                        {this.props.children}
                    </div>
                    <div className="content-sup">
                    </div>
                </div>
                <Footer/>
            </div>

        );
    }
}

Template.contextTypes = {
    store: React.PropTypes.object,
    router: React.PropTypes.object
};

export default connect(
    state => (state)
)(Template);
