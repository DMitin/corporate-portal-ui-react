import React, { Component } from 'react';
import SearchResult from './SearchResult';
import Sort from './Sort';
import Pagination from './Pagination';
import { connect } from 'react-redux';

import api from './api.js';
import actions from './actions';



class Search extends Component {

	constructor(props) {
		super(props);
		this.state = {
	      searchResults: []
	    };
	}
	componentDidMount() {
		const {store} = this.context;
		store.dispatch(actions.search());
	}

	testMethod() {
		console.log(store);
	}

	render(){
		const {store} = this.context;
		const searchItems = this.props.searchResults || [];
		const pageCount = this.props.pageCount || 0;
		const totalRecords = this.props.totalRecords || 0;
		const currentPage = this.props.currentPage || 0;

		console.log(this.state);
		return (
			<div>
				<h1>Сотрудники</h1>

				<div className="search">
					<input type="text" placeholder="Имя / Фамилия "

						   onChange={(e)=>{
								var searchStr = e.target.value;
								store.dispatch(actions.search(searchStr));
						   }}
					/> <span className="icn-search"></span>
				</div>

				<div className="advanced-search">
					<a><u>Расширенный поиск</u></a>
					<div className="inner">

					</div>
				</div>
				<div className="search-results">

					<Sort/>

					<SearchResult groups={searchItems} />
				</div>


				<Pagination pageCount={pageCount}
							totalRecords={totalRecords}
							currentPage={currentPage}/>
			</div>
		);
	}
}

Search.contextTypes = {
	store: React.PropTypes.object
};

//export default Search;
export default connect(
	state => (state.search)
)(Search);
