/**
 * Created by Denis Mitin on 19.05.2016.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import actions from './actions';

class Sort extends Component {


    componentWillMount () {
        console.log("Sort componentDidMount");
        this.setState({desc: this.props.desc});
    }


    handleClick (event) {
        //console.log("handleClick");
        let toggleDesc = !this.state.desc;
        this.setState({desc: toggleDesc});
        this.setClassName(toggleDesc);
        this.context.store.dispatch(actions.changeDesc());
    }

    componentWillReceiveProps (nextProps) {
        console.log("Sort componentWillReceiveProps");
        this.setState({desc: this.props.desc});
        this.setClassName(this.props.desc);
    }

    setClassName(desc) {
        console.log("desc: " + desc);
        let className = desc? "icn-sort-amount-desc": "icn-sort-amount-asc";
        let fullClassName = "sort pull-right " + className;
        console.log(fullClassName);
        this.setState({className: fullClassName});
    }

    render(){

        return (
            <div className="tool">
                <a className={this.state.className} onClick={() => this.handleClick()} />
            </div>
        )
    }


}

Sort.contextTypes = {
    store: React.PropTypes.object
};



export default connect(
    state => (state.search)
)(Sort);