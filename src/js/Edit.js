import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { DateField } from 'react-date-picker'
import { DatePicker } from 'react-date-picker'
// import 'react-date-picker/index.css'

import * as profileActions from './actions/profile';
import api from './api';

class Edit extends Component {

    componentDidMount() {
        this.setState(this.props.profile);
    }


    componentWillReceiveProps (nextProps) {
        console.log("componentWillUpdate");
        this.setState(nextProps.profile);
    }

    render() {

        const {store} = this.context;
        let state = this.state || {};
        let social = state.social || {};

        let imagePath;
        let imageProfile = state.image;
        if (imageProfile) {
            imagePath = state.image;
        } else {
            imagePath = "/static/Content/images/no-image.png"
        }

        return(

        <div className="edit-view">

        <h2>Редактирование профиля</h2>

            <div className="search-item">
                <div>
                    <span>
                        <img src={imagePath}/>
                    </span>
                    <input type="file"  onChange={(e)=>{

                        var body = new FormData();
                        body.append('file', e.target.files[0]);
                        store.dispatch(profileActions.uploadImage(body));
                    }}/>
                </div>
                <div className="personal-info">
                    <div className="field error">
                        <input id="lastname-input" type="text" value={state.lastName} placeholder="Фамилия"
                               onChange={(e)=>{
								    this.setState({lastName: e.target.value});
						       }}
                        />
                        <span className="validation-msg">Что-то пошло не так!</span>
                    </div>
                    <div className="field">
                        <input id="firstname-input" type="text" value={state.firstName} placeholder="Имя"
                               onChange={(e)=>{
								    this.setState({firstName: e.target.value});
						       }}
                        />
                    </div>
                    <div className="field">
                        <input type="text" value={state.middleName} placeholder="Отчество"
                               onChange={(e)=>{
								    this.setState({middleName: e.target.value});
						       }}
                        />
                    </div>

                        <DateField
                            dateFormat="YYYY-MM-DD"
                            forceValidDate = {true}
                            updateOnDateClick = {true}
                            collapseOnDateClick = {true}
                            defaultValue={1472039399149}
                            expandOnFocus = {false}
                            showClock={false}
                            placeholder="дд.мм.гг"
                            >
                            <DatePicker
                              navigation={true}
                              locale="en"
                              forceValidDate={true}
                              highlightWeekends={true}
                              highlightToday={false}
                              weekNumbers={false}
                              weekStartDay={1}
                              footer={true}/>
                            </DateField>

                </div>
            </div>

          <div className="control">
              <label className="control__label">Должность:</label>
              <div className="field">
                <input type="text" value="xxx" placeholder="Должность" readOnly />
              </div>
            </div>

            <div className="control">
              <label className="control__label">Почта:</label>
              <div className="field">
                <input type="text" value={state.email} placeholder="Email"
                       onChange={(e)=>{this.setState({email: e.target.value});}}
                />
              </div>
            </div>

            <div className="control">
              <label className="control__label">Skype:</label>
              <div className="field">
               <input type="text" value={state.skype} placeholder="Skype"
                                    onChange={(e)=>{this.setState({skype: e.target.value});}}/></div>
            </div>
            <div className="control">
              <label className="control__label">Отдел:</label>
              <div className="field">
              <input type="text" value="xxx " placeholder="Отдел" readOnly />
              </div>
            </div>
            <div className="control">
              <label className="control__label">Проекты:</label>
              <div className="field">
               <input type="text" value="xxx" placeholder="Проекты" readOnly  />
               </div>
            </div>
            <div className="control">
              <label className="control__label">Локация:</label>
              <div className="field">
               <a className="icn-map-marker"><u>xxx</u></a>
               </div>
            </div>



            <div className="block-title">Дополнительная информация</div>

            <ul className="social">
                <li>
                    <span className="icn-twitter">
                        <span className="field">
                            <input type="text" value={social.twitter} placeholder="twitter"
                                   onChange={(e)=>{
                                        let social = Object.assign({}, this.state.social, {
                                            twitter: e.target.value
                                        });
                                        this.setState({social: social});
                                   }}
                            />
                        </span>
                    </span>
                </li>
                <li>
                    <span className="icn-facebook">
                        <span className="field">
                            <input type="text" value={social.facebook} placeholder="facebook"
                                   onChange={(e)=>{
                                        let social = Object.assign({}, this.state.social, {
                                            facebook: e.target.value
                                        });
                                        this.setState({social: social});
                                   }}
                            />
                        </span>
                    </span>
                </li>
                <li><span className="icn-google-plus">
                <span className="field"><input type="text" value={social.googlePlus} placeholder="google Plus"
                                   onChange={(e)=>{
                                       let social = Object.assign({}, this.state.social, {
                                            googlePlus: e.target.value
                                       });
                                        this.setState({social: social});
                                   }}/></span></span></li>
                <li><span className="icn-linkedin">
                <span className="field"><input type="text" value={social.linkedin} placeholder="linkedin"
                                    onChange={(e)=>{
                                        let social = Object.assign({}, this.state.social, {
                                            linkedin: e.target.value
                                        });
                                        this.setState({social: social});
                                   }}/></span></span></li>
                <li><span className="icn-instagram">
                <span className="field"><input type="text" value={social.instagram} placeholder="instagram"
                                     onChange={(e)=>{
                                        let social = Object.assign({}, this.state.social, {
                                            instagram: e.target.value
                                        });
                                        this.setState({social: social});
                                   }}/></span></span></li>
                <li><span className="icn-vk">
                <span className="field"><input type="text" value={social.vk} placeholder="vk"
                                    onChange={(e)=>{
                                        let social = Object.assign({}, this.state.social, {
                                            vk: e.target.value
                                        });
                                        this.setState({social: social});
                                   }}/></span></span></li>
            </ul>


            <div className="block-title">Навыки</div>

            <textarea value="xxx" readOnly></textarea>


            <div className="action">
                <button id="edit-card-save-button" className="btn btn-primary"
                        onClick={(e)=>{
                            //console.log(this.state);
                            store.dispatch(profileActions.updateProfile(state));
                        }}>Сохранить
                </button>
                <Link to="/card" className="btn btn-default"><u>Отмена</u></Link>
            </div>

        </div>
        )
    }
}

//export default Edit;
Edit.contextTypes = {
    store: React.PropTypes.object
};

//export default Search;
export default connect(
    state => (state.watchProfile), {}, (stateProps, dispatchProps, ownProps) => (
        Object.assign({}, ownProps, {
            profile: stateProps
        }, dispatchProps)
    )
)(Edit);
