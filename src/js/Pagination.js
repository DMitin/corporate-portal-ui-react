import React, { Component } from 'react';
import { connect } from 'react-redux';
import Page from './Page';

import actions from './actions';

//let pageSize = 5;

class Pagination extends Component {

	componentDidMount() {
		let pageSize = this.props.search.pageSize;
		this.setState({pageSize});
		//store.dispatch(actions.search());
	}

	render(){
		const {store} = this.context;
		let currentPage = this.props.currentPage;
		let pageSize = this.props.search.pageSize;
		let state = this.state || {};
		//this.state.pageSize = pageSize;
		//this.setState({pageSize});

		/*
		var groups = this.props.groups.map((group) => {
			return <SearchGroup key={group.letter}
						letter={group.letter}
						items={group.items} />
		});
		*/
		var pages = [];
		let totalRecords = this.props.totalRecords;
		let pageCount = this.props.pageCount;
		for (let i = 0; i < pageCount; i ++) {
			if (i === currentPage) {
				pages.push(<Page key={i} active={true} number={i} />)	
			} else {
				pages.push(<Page key={i} number={i} />)	
			}
		}

		let startRecord = currentPage * pageSize;
		let endRedcord;
		let therotheticalPageCount = (currentPage + 1) * pageSize;
		if (therotheticalPageCount > totalRecords) {
			endRedcord = startRecord + (totalRecords % pageSize)
		} else {
			endRedcord = therotheticalPageCount;
		}

		return (
			<div className="pagination">
				<ul>
					<li>
						<a className="icn-angle-double-left"
								onClick={(e)=>{
									store.dispatch(actions.firstPage());
								}}>
						</a>
					</li>
					<li>
                        <a className="icn-angle-left"
                           onClick={(e)=>{
									store.dispatch(actions.previousPage());
								}}
                        />
                    </li>
					    {pages}
					<li>
						<a className="icn-angle-right"
                           onClick={(e)=>{
									store.dispatch(actions.nextPage());
								}}
						/>
					</li>
					<li>
						<a className="icn-angle-double-right"
								onClick={(e)=>{
									store.dispatch(actions.lastPage());
								}}>
						</a>
					</li>
					<li>Показывать по
						<input type="text" value={state.pageSize}
							   onChange={(e)=>{

								let pageSize = e.target.value;
								if (pageSize) {
									let pageSizeInt = parseInt(pageSize);
									if (pageSizeInt) {
										if (pageSizeInt > 50) {
											pageSizeInt = 50;
										}
										this.setState({pageSize: pageSizeInt});
									}
								} else {
									this.setState({pageSize: pageSize});
								}
						   }}
						   onKeyPress={(e)=>{
						   		if (e.key === 'Enter') {
								  //console.log("onKeyPress: " +  state.pageSize);
								  store.dispatch(actions.changePageSize(state.pageSize));
								}
						   }}
						/>
						на странице
					</li>
					<li className="pull-right">{startRecord}-{endRedcord} из {totalRecords}</li>
				</ul>
			</div>
		);

		/*
		return (
			<div className="pagination">
				<ul>
					<li><span className="icn-angle-double-left"></span></li>
					<li><a className="icn-angle-left"></a></li>
					<li><a className="page">1</a></li>
					<li><a className="page active">2</a></li>
					<li><a className="page">3</a></li>
					<li><a className="page">4</a></li>
					<li><a className="page">5</a></li>				
					<li><a className="icn-angle-right"></a></li>
					<li><a className="icn-angle-double-right"></a></li>
					<li>Показывать по <input type="text"/> на странице</li>
					<li className="pull-right">15-30 из 210</li>
				</ul>
			</div>
		)
		*/
	}
};

//export default Pagination;
Pagination.contextTypes = {
	store: React.PropTypes.object
};



export default connect(
	state => (state)
)(Pagination);
