import React, { Component } from 'react';
import SearchGroup from './SearchGroup';

class SearchResult extends Component {
	render(){

		var groups = this.props.groups.map((group) => {
			return <SearchGroup key={group.letter}
						letter={group.letter}
						items={group.items} />
		});

		console.log(groups);

	return (
		<div>
			{groups}
		</div>
	);
	}
}
export default SearchResult;