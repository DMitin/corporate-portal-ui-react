import React, { Component } from 'react';
import { Link } from 'react-router';

class SearchItem extends Component {
	render(){
		return (
			<div className="search-item">
					<div>
						<span>
							<img src={this.props.image}></img>
						</span>
					</div>
					<div className="personal-info">
						<Link className="name" to={"/card/" + this.props.id}>{this.props.name}</Link>
						<div className="position">{this.props.position}</div>
						<div className="email"><span>email</span> <a href="{this.props.email}">{this.props.email}</a></div>
						<div className="skype"><span>skype</span> {this.props.skype}</div>
					</div>
					<div className="align-center map">
						<a className="icn-map-marker"><u>{this.props.room}</u></a>
					</div>
				</div>
		)
	}
};

export default SearchItem;