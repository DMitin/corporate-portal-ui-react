var gulp = require("gulp");
var sass = require("gulp-sass");

function swallowError (error) {
  console.log(error.toString());
  this.emit('end');
}

gulp.task("default", function(){
    gulp.src('./src/scss/styles.scss')
        .pipe(sass())
        .on('error', swallowError)
        .pipe(gulp.dest('./public/'));
});

gulp.task('watch', function(){
   gulp.watch('./src/scss/**/*.*', ['default']);
});
